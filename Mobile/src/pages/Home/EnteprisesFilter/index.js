import React, {useState} from 'react';
import {
  View,
  TextInput,
  ActivityIndicator,
  Text,
  FlatList,
  Image,
} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import {EnterprisesFilterRequest} from '../../../store/modules/enterprises/actions';

Icon.loadFont();

export default function EnterprisesFilter() {
  const [name, setName] = useState('');
  const [type, setType] = useState('');

  const dispatch = useDispatch();
  const data = useSelector((state) => state.enterprises.filter);
  const loading = useSelector((state) => state.enterprises.loadingFilter);

  function handleFilter() {
    dispatch(EnterprisesFilterRequest(name, type));
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View>
          <TextInput
            placeholder="Nome Empresa"
            style={styles.input}
            placeholderTextColor="#353b48"
            value={name}
            onChangeText={setName}
          />
          <TextInput
            placeholder="Tipo"
            style={styles.input}
            placeholderTextColor="#353b48"
            value={type}
            onChangeText={setType}
            keyboardType="number-pad"
          />
        </View>

        <RectButton style={styles.btnSearch} onPress={handleFilter}>
          <Icon name="search" color="#f5f6fa" size={25} />
        </RectButton>
      </View>

      {loading ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="#353b48" />
        </View>
      ) : (
        <>
          {data.length === 0 ? (
            <View style={styles.indicator}>
              <Text style={styles.textIndicator}>
                Nenhum Empresa Foi Encontrada !
              </Text>
            </View>
          ) : (
            <FlatList
              data={data}
              style={styles.list}
              keyExtractor={(item) => item.id}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <View style={styles.listContainer}>
                  <Image
                    source={{
                      uri: `https://empresas.ioasys.com.br/${item.photo}`,
                    }}
                    style={styles.listImage}
                  />
                  <View style={styles.listContent}>
                    <Text style={styles.listTitle}>{item.enterprise_name}</Text>
                    <Text style={styles.listDescription}>
                      {item.description}
                    </Text>
                  </View>
                </View>
              )}
            />
          )}
        </>
      )}
    </View>
  );
}
