import {StyleSheet} from 'react-native';
import { scale } from 'react-native-size-matters'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 10,
  },
  btnSearch: {
    height: 40,
    width: 80,
    borderRadius: 13,
    backgroundColor: '#353b48',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 40,
    width: 260,
    borderWidth: 1,
    borderColor: '#353b48',
    marginTop: 10,
    borderRadius: 20,
    paddingLeft: 10,
    color: '#353b48',
    fontSize: 16,
    fontWeight: 'bold',
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textIndicator: {
    fontSize: 16,
    color: '#353b48',
    fontWeight: 'bold',
  },
  list: {
    marginTop: 10,
    marginHorizontal: 10,
  },
  listContainer: {
    alignSelf: 'center',
    width: '90%',
    backgroundColor: '#353b48',
    borderRadius: 20,
    marginTop: 10,
    flexDirection: 'row',
    padding: 15,
  },
  listContent: {
    marginLeft: 10,
  },
  listImage: {
    height: 50,
    width: 50,
    borderRadius: 10,
  },
  listTitle: {
    fontSize: 14,
    color: '#f5f6fa',
    fontWeight: 'bold',
  },
  listDescription: {
    fontSize: 12,
    color: '#f5f6fa',
    fontWeight: '800',
    textAlign: 'justify',
    width: scale(220),
    marginTop: 5,
  },
});

export default styles;
