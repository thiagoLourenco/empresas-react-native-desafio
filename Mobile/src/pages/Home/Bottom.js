import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icons from 'react-native-vector-icons/FontAwesome';

import Routes from './index';
import UserInfo from './UserInfo';

const Tab = createBottomTabNavigator();

Icons.loadFont();

const icons = {
  Routes: {
    lib: Icons,
    name: 'home',
  },
  UserInfo: {
    lib: Icons,
    name: 'users',
  },
};

export default function BottomTab() {
  return (
    <Tab.Navigator
      initialRouteName="Routes"
      screenOptions={({route, navigation}) => ({
        tabBarIcon: ({color, size, focused}) => {
          const {lib: Icon, name} = icons[route.name];
          return <Icon name={name} color={color} size={size} />;
        },
      })}
      tabBarOptions={{
        style: {
          backgroundColor: '#353b48',
          paddingBottom: 5,
        },
        activeTintColor: '#f5f6fa',
        inactiveTintColor: '#cccccc',
      }}>
      <Tab.Screen
        name="Routes"
        component={Routes}
        options={{title: 'Empresas'}}
      />
      <Tab.Screen
        name="UserInfo"
        component={UserInfo}
        options={{title: 'Perfil'}}
      />
    </Tab.Navigator>
  );
}
