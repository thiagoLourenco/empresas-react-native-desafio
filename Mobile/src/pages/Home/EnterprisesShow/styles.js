import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 300,
    width: '100%',
  },
  image: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
  },
  headerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  title: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: 'bold',
  },
  textValor: {
    fontSize: 18,
    color: '#16a085',
    fontWeight: 'bold',
  },
  content: {
    margin: 10,
  },
  contentDescription: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  contentText: {
    fontSize: 16,
    color: '#353b48',
    fontWeight: '800',
    textAlign: 'justify',
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSearch: {
    marginRight: 10,
  },
  enterpriseType: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  enterpriseTypeLabel: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: 'bold',
  },
  enterpriseTypeText: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: '600',
  },
});

export default styles;
