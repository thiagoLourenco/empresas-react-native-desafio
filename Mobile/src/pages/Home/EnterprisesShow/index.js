import React, {useEffect, useCallback} from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import {useFocusEffect, useRoute} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {ScrollView} from 'react-native-gesture-handler';

import styles from './styles';
import {EnterprisesShowRequest} from '../../../store/modules/enterprises/actions';

export default function EnterprisesShow() {
  const routes = useRoute();
  const dispatch = useDispatch();

  const id = routes.params.id;

  const data = useSelector((state) => state.enterprises.dataShow);
  const loading = useSelector((state) => state.enterprises.loadingShow);

  useEffect(() => {
    dispatch(EnterprisesShowRequest(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useFocusEffect(
    useCallback(() => {
      dispatch(EnterprisesShowRequest(id));
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]),
  );

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="#353b48" />
        </View>
      ) : (
        <>
          {data.length === 0 ? null : (
            <ScrollView>
              <View style={styles.header}>
                <Image
                  source={{
                    uri: `https://empresas.ioasys.com.br/${data.enterprise.photo}`,
                  }}
                  style={styles.image}
                />
                <View style={styles.headerView}>
                  <Text style={styles.title}>
                    {data.enterprise.enterprise_name}
                  </Text>
                  <Text style={styles.textValor}>
                    Valor: {parseFloat(data.enterprise.value).toFixed(1)}
                  </Text>
                </View>
              </View>

              <View style={styles.enterpriseType}>
                <Text style={styles.enterpriseTypeLabel}>Tipo De Empresa: </Text>
                <Text style={styles.enterpriseTypeText}>
                  {data.enterprise.enterprise_type.enterprise_type_name}
                </Text>
              </View>

              <View style={styles.content}>
                <Text style={styles.contentDescription}>Descrição</Text>
                <Text style={styles.contentText}>
                  {data.enterprise.description}
                </Text>
              </View>
            </ScrollView>
          )}
        </>
        
      )}
    </View>
  );
}
