import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    height: 350,
    width: 315,
    borderRadius: 30,
    backgroundColor: '#fff',
    shadowColor: '#ccc',
    shadowOffset: {
      height: 8,
      width: 8,
    },
    shadowOpacity: 7,
    shadowRadius: 7,
    elevation: 7,

    // marginTop: scale(10),
  },
  contentHeader: {
    alignItems: 'center',
    marginTop: 10,
  },
  contentTitle: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: 'bold',
  },
  contentTextEmail: {
    fontSize: 16,
    color: '#353b48',
    fontWeight: '600',
  },
  country: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#353b48',
    marginLeft: 20,
  },
  textCountry: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#353b48',
  },
  contentView: {
    marginTop: 30,
    // marginLeft: 20,
  },
  viewValues: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  textContentViewValue: {
    fontSize: 16,
    color: 'green',
    fontWeight: '800',
    marginLeft: 5,
  },
  viewBalance: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },

  textBalance: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#353b48',
  },
  textContentViewBalance: {
    fontSize: 20,
    color: 'green',
    fontWeight: '800',
    marginLeft: 5,
  },
  btnLogout: {
    height: 50,
    width: '90%',
    alignSelf: 'center',
    marginTop: 50,
    backgroundColor: '#353b48',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 19,
  },
  btnLogoutText: {
    fontSize: 16,
    color: '#f5f6fa',
    fontWeight: 'bold',
  },
});

export default styles;
