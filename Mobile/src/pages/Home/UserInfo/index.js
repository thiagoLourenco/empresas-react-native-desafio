import React from 'react';
import {View, Text} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RectButton} from 'react-native-gesture-handler';

import styles from './styles';
import {logout} from '../../../store/modules/auth/actions';

export default function UserInfo() {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.auth.user);

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.contentHeader}>
          <Text style={styles.contentTitle}>{user.investor.investor_name}</Text>
          <Text style={styles.contentTextEmail}>{user.investor.email}</Text>
        </View>

        <View style={styles.contentView}>
          <Text style={styles.country}>
            {user.investor.city}/{user.investor.country}
          </Text>

          <View style={styles.viewValues}>
            <Text style={styles.textCountry}>
              Total de Empresa no Portfolio:
            </Text>
            <Text style={styles.textContentViewValue}>
              {user.investor.portfolio.enterprises_number}
            </Text>
          </View>

          <View style={styles.viewValues}>
            <Text style={styles.textCountry}>Valor Do Portfolio:</Text>
            <Text style={styles.textContentViewValue}>
              {user.investor.portfolio_value}
            </Text>
          </View>

          <View style={styles.viewBalance}>
            <Text style={styles.textBalance}>Saldo: </Text>
            <Text style={styles.textContentViewBalance}>
              {user.investor.balance}
            </Text>
          </View>
        </View>

        <RectButton style={styles.btnLogout} onPress={() => dispatch(logout())}>
          <Text style={styles.btnLogoutText}>Sair</Text>
        </RectButton>
      </View>
    </View>
  );
}
