import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import {RectButton} from 'react-native-gesture-handler';

import Enterprises from './Enterprises';
import EnterprisesShow from './EnterprisesShow';
import EnterprisesFilter from './EnteprisesFilter';
import styles from './EnterprisesShow/styles';

const Stack = createStackNavigator();

Icon.loadFont();

export default function Routes() {
  return (
    <Stack.Navigator initialRouteName="Enterprises">
      <Stack.Screen
        name="Enterprises"
        component={Enterprises}
        options={({navigation}) => ({
          headerStyle: {backgroundColor: '#353b48'},
          headerTintColor: '#f5f6fa',
          headerTitle: 'Lista de Empresas',
          headerTitleAlign: 'center',
          headerRight: () => {
            return (
              <RectButton
                style={styles.btnSearch}
                onPress={() => navigation.navigate('EnterprisesFilter')}>
                <Icon name="search" color="#f5f6fa" size={25} />
              </RectButton>
            );
          },
          animationEnabled: true,
        })}
      />

      <Stack.Screen
        name="EnterprisesShow"
        component={EnterprisesShow}
        options={{
          headerStyle: {backgroundColor: '#353b48'},
          headerTintColor: '#f5f6fa',
          headerTitle: 'Detalhes',
          headerTitleAlign: 'center',
        }}
      />

      <Stack.Screen
        name="EnterprisesFilter"
        component={EnterprisesFilter}
        options={{
          headerStyle: {backgroundColor: '#353b48'},
          headerTintColor: '#f5f6fa',
          headerTitle: 'Filtro de Empresa',
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
}
