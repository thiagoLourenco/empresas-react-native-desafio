import React, {useEffect, useCallback} from 'react';
import {View, Text, FlatList, Image, ActivityIndicator} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import {EnterprisesRequest} from '../../../store/modules/enterprises/actions';

export default function Enterprises() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const data = useSelector((state) => state.enterprises.data);
  const loading = useSelector((state) => state.enterprises.loading);

  useEffect(() => {
    dispatch(EnterprisesRequest());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useFocusEffect(
    useCallback(() => {
      dispatch(EnterprisesRequest());
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="#353b48" />
        </View>
      ) : (
        <FlatList
          style={styles.list}
          showsVerticalScrollIndicator={false}
          data={data}
          numColumns={2}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => (
            <RectButton
              style={styles.listCard}
              onPress={() =>
                navigation.navigate('EnterprisesShow', {id: item.id})
              }>
              <Image
                style={styles.listCardImage}
                source={{
                  uri: `https://empresas.ioasys.com.br/${item.photo}`,
                }}
              />
              <Text style={styles.listCardTitle}>{item.enterprise_name}</Text>
              <Text style={styles.listCardDescription}>
                {item.description.length < 98
                  ? item.description
                  : item.description.substring(0, 120)}
              </Text>
              <Text style={styles.listCardFooter}>
                {item.city}/{item.country}
              </Text>
            </RectButton>
          )}
        />
      )}
    </View>
  );
}
