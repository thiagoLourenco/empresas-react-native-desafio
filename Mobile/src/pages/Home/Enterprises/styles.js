import {StyleSheet} from 'react-native';
import {scale} from 'react-native-size-matters'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    color: '#353b48',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
  },
  listCard: {
    backgroundColor: '#353b48',
    width: scale(150),
    height: scale(150),
    borderRadius: 20,
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {
      height: 4,
      width: 0,
    },
    shadowOpacity: 0.6,
    shadowRadius: 10,
    elevation: 8,
    padding: 10,
  },
  listCardTitle: {
    fontSize: 14,
    color: '#fff',
    fontWeight: 'bold',
  },
  listCardDescription: {
    fontSize: 12,
    color: 'rgba(245, 246, 250, 0.9)',
    fontWeight: 'bold',
    marginTop: 5,
  },
  listCardFooter: {
    fontSize: 10,
    color: 'rgba(245, 246, 250, 0.8)',
    fontWeight: 'bold',
    marginTop: 5,
  },
  listCardImage: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 20,
  },
  list: {
    flexGrow: 1,
    alignSelf: 'center',
    marginTop: 20,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
