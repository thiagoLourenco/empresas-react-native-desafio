import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Auth from './Auth'

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Auth" component={Auth} options={{ headerShown: false }}/>
    </Stack.Navigator>
  )
}