import React, {useState} from 'react';
import {View, Text, Image, TextInput, ActivityIndicator} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Formik } from 'formik'
 import * as Yup from 'yup';

import styles from './styles';
import logo from '../../../assets/logo_ioasys.png';
import {signInRequest} from '../../../store/modules/auth/actions';

Icon.loadFont();

export default function Auth() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  const loading = useSelector((state) => state.auth.loading);

  const authSchema = Yup.object().shape({
    email: Yup.string().email('Email Inválido').required('Required'),
    password: Yup.string().required('Required')
  })

  function handleSignIn(values) {
    dispatch(signInRequest(values.email, values.password));
  }

  return (
    <View style={styles.container}>
      <Image source={logo} />

      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values => handleSignIn(values)}
        validationSchema={authSchema}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <>
            <View style={[styles.viewInput, { borderColor: errors.email && touched.email ? 'red' : '#353b48' }]}>
              <Text>{ errors.email && touched.email}</Text>
              <Icon name="email" color={errors.email && touched.email ? 'red' : '#353b48' } size={25}/>
              <TextInput
                style={styles.input}
                placeholder="Email"
                placeholderTextColor={errors.email && touched.email ? 'red' : '#353b48'}
                autoCapitalize="none"
                keyboardType="email-address"
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
              />
            </View>
      
            <View style={[styles.viewInput,{ borderColor: errors.password && touched.password ? 'red' : '#353b48' }]}>
              <Icon name="lock" color={errors.password && touched.password ? 'red' : '#353b48' } size={25}/>
              <TextInput
                style={styles.input}
                placeholder="********"
                placeholderTextColor={errors.password && touched.password ? 'red' : '#353b48'}
                secureTextEntry
                value={values.password}
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
              />
            </View>
            <RectButton style={styles.btn} onPress={handleSubmit}>
              {loading ? (
                <ActivityIndicator size="small" color="#f5f6fa" />
              ) : (
                <Text style={styles.btnText}>Entrar</Text>
              )}
            </RectButton>
          </>
      )}
      
      </Formik>
      
      

      
    </View>
  );
}
