import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f6fa',
  },
  viewInput: {
    flexDirection: "row",
    width: 320,
    height: 48,
    borderWidth: 1,
    borderColor: '#353b48',
    marginTop: 20,
    paddingLeft: 10,
    alignItems: "center",
    borderRadius: 10,
  },
  input: {
    color: '#353b48',
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 5,
    width:"100%"
  },
  btn: {
    height: 45,
    width: 320,
    backgroundColor: '#353b48',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  btnText: {
    fontSize: 16,
    color: '#f5f6fa',
    fontWeight: '800',
  },
});

export default styles;
