import {all} from 'redux-saga/effects';

import authSaga from './auth/sagas';
import enterprisesSaga from './enterprises/sagas';

export default function* rootSaga() {
  return yield all([authSaga, enterprisesSaga]);
}
