export function signInRequest(email, password) {
  return {
    type: "@sign/SIGN_IN_REQUEST",
    payload: {email, password},
  };
}

export function signInSuccess(data, accessToken, client, uid) {
  return {
    type: "@sign/SIGN_IN_SUCCESS",
    payload: {data, accessToken, client, uid},
  };
}

export function signInFailure() {
  return {
    type: "@sign/SIGN_IN_FAILURE",
  };
}

export function logout() {
  return {
    type: "@auth/SIGN_OUT",
  };
}
