import {all, call, takeLatest, put} from "redux-saga/effects";
import {showMessage} from 'react-native-flash-message';

import api from "../../../services/api";

import {signInSuccess, signInFailure} from "./actions";

export function* SignIn({payload}) {
  try {
    const data = {
      email: payload.email,
      password: payload.password,
    };

    const response = yield call(api.post, "users/auth/sign_in", data);

    const accessToken = response.headers["access-token"];
    const {client} = response.headers;
    const {uid} = response.headers;

    api.defaults.headers["access-token"] = accessToken;
    api.defaults.headers.client = client;
    api.defaults.headers.uid = uid;

    yield put(signInSuccess(response.data, accessToken, client, uid));
  } catch (error) {
    showMessage({
      type: "danger",
      message: "Erro ao Realizar Login. Verificar os dados, por favor!",
    });

    yield put(signInFailure());
  }
}

export function setToken({payload}) {
  if (!payload) {
    return;
  }

  const {accessToken, client, uid} = payload.auth;

  if (accessToken || client || uid) {
    api.defaults.headers["access-token"] = accessToken;
    api.defaults.headers.client = client;
    api.defaults.headers.uid = uid;
  }
}

export default all([
  takeLatest("persist/REHYDRATE", setToken),
  takeLatest("@sign/SIGN_IN_REQUEST", SignIn),
]);
