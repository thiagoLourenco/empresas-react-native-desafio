import produce from "immer";

const INITIAL_STATE = {
  loading: false,
  signed: false,
  user: null,
  accessToken: null,
  client: null,
  uid: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case "@sign/SIGN_IN_REQUEST": {
        draft.loading = true;
        draft.signed = false;
        break;
      }

      case "@sign/SIGN_IN_SUCCESS": {
        draft.user = action.payload.data;
        draft.accessToken = action.payload.accessToken;
        draft.client = action.payload.client;
        draft.uid = action.payload.uid;
        draft.loading = false;
        draft.signed = true;
        break;
      }

      case "@sign/SIGN_IN_FAILURE": {
        draft.loading = false;
        draft.signed = false;
        break;
      }

      case "@auth/SIGN_OUT": {
        draft.signed = false;
        break;
      }

      default:
        draft.loading = false;
        break;
    }
  });
}
