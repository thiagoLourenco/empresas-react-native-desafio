import produce from "immer";

const INITIAL_STATE = {
  loading: false,
  loadingShow: false,
  loadingFilter: false,
  data: [],
  dataShow: [],
  filter: [],
};

export default function enterprises(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case "@enterprises/ENTERPRISES_REQUEST": {
        draft.loading = true;
        break;
      }

      case "@enterprises/ENTERPRISES_SUCCESS": {
        draft.data = action.payload.data;
        draft.loading = false;
        break;
      }

      case "@enterprises/ENTERPRISES_FAILURE": {
        draft.loading = false;
        break;
      }

      case "@enterprises/ENTERPRISES_SHOW_REQUEST": {
        draft.loadingShow = true;
        break;
      }

      case "@enterprises/ENTERPRISES_SHOW_SUCCESS": {
        draft.dataShow = action.payload.data;
        draft.loadingShow = false;
        break;
      }

      case "@enterprises/ENTERPRISES_SHOW_FAILURE": {
        draft.loadingShow = false;
        break;
      }

      case "@enterprises/ENTERPRISES_FILTER_REQUEST": {
        draft.loadingFilter = true;
        break;
      }

      case "@enterprises/ENTERPRISES_FILTER_SUCCESS": {
        draft.filter = action.payload.data;
        draft.loadingFilter = false;
        break;
      }

      case "@enterprises/ENTERPRISES_FILTER_FAILURE": {
        draft.loadingFilter = false;
        break;
      }

      default:
        draft.loading = false;
        draft.loadingShow = false;
        draft.loadingFilter = false;
        break;
    }
  });
}
