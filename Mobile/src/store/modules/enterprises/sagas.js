import {all, call, put, takeLatest} from 'redux-saga/effects';
import {showMessage} from 'react-native-flash-message';

import api from '../../../services/api';

import {
  EnterprisesSuccess,
  EnterprisesShowSuccess,
  EnterprisesFilterSuccess,
  EnterprisesShowFailure,
  EnterprisesFilterFailure,
  EnterprisesFailure,
} from './actions';

export function* getEnterprises() {
  try {
    const response = yield call(api.get, 'enterprises');

    yield put(EnterprisesSuccess(response.data.enterprises));
  } catch (error) {
    showMessage({
      type: 'danger',
      message: 'Ocorreu algum error, inesperado.',
    });
    yield put(EnterprisesFailure());
  }
}

export function* showEnterprises({payload}) {
  try {
    const response = yield call(api.get, `enterprises/${payload.id}`);

    yield put(EnterprisesShowSuccess(response.data));
  } catch (error) {
    showMessage({
      type: 'danger',
      message: 'Ocorreu algum error, inesperado.',
    });
    yield put(EnterprisesShowFailure());
  }
}

export function* filterEnterprises({payload}) {
  try {
    const response = yield call(
      api.get,
      `enterprises?enterprise_types=${payload.type}&name=${payload.name}`,
    );

    yield put(EnterprisesFilterSuccess(response.data.enterprises));
  } catch (error) {
    showMessage({
      type: 'danger',
      message: 'Ocorreu algum error, inesperado.',
    });
    yield put(EnterprisesFilterFailure());
  }
}

export default all([
  takeLatest("@enterprises/ENTERPRISES_FILTER_REQUEST", filterEnterprises),
  takeLatest("@enterprises/ENTERPRISES_REQUEST", getEnterprises),
  takeLatest("@enterprises/ENTERPRISES_SHOW_REQUEST", showEnterprises),
]);
