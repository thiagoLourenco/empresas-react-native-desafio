export function EnterprisesRequest() {
  return {
    type: "@enterprises/ENTERPRISES_REQUEST",
  };
}

export function EnterprisesSuccess(data) {
  return {
    type: "@enterprises/ENTERPRISES_SUCCESS",
    payload: {data},
  };
}

export function EnterprisesFailure() {
  return {
    type: "@enterprises/ENTERPRISES_FAILURE",
  };
}

export function EnterprisesShowRequest(id) {
  return {
    type: "@enterprises/ENTERPRISES_SHOW_REQUEST",
    payload: {id},
  };
}

export function EnterprisesShowSuccess(data) {
  return {
    type: "@enterprises/ENTERPRISES_SHOW_SUCCESS",
    payload: {data},
  };
}

export function EnterprisesShowFailure() {
  return {
    type: "@enterprises/ENTERPRISES_SHOW_FAILURE",
  };
}

export function EnterprisesFilterRequest(name, type) {
  return {
    type: "@enterprises/ENTERPRISES_FILTER_REQUEST",
    payload: {name, type},
  };
}

export function EnterprisesFilterSuccess(data) {
  return {
    type: "@enterprises/ENTERPRISES_FILTER_SUCCESS",
    payload: {data},
  };
}

export function EnterprisesFilterFailure() {
  return {
    type: "@enterprises/ENTERPRISES_FILTER_FAILURE",
  };
}
