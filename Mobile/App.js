import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import FlashMessage from 'react-native-flash-message'

import Routes from './src/pages';
import {persistor, store} from './src/store';

export default function App() {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Routes />
          <StatusBar backgroundColor="#353b48" barStyle="light-content" />
          <FlashMessage animated={true} floating={true} position="top"/>
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
}
