# Desafio React Native - ioasys
 Realizado Desafio proposto da Ioasys, o descritivo do desavio está dentro 
 da pasta Mobile, no Arquivo README.md

# Executar Projeto
cd Mobile/
yarn ou npm install

* Android 
  - npx react-native run-android

* IOS
  - cd ios/
  - pod install 
  - cd ..
  - npx react-native run-ios
  
# Bibliotecas Utilizadas
  * React-Navigation V5: Biblioteca utilizada para realizar o processo de navegação
    entre as telas
  * @react-native-async-storage/async-storage: Realizada para salvar os dados de 
    client, uid, access_token. 
  * redux: Realizar o gerenciamento dos estados.
  * redux-saga: Utilizado para comunicação com a API.
  * axios: Fazer a chamada da api
  * formik: Validar e gerenciar melhor os formulários.
  * react-native-flash-message: Mostrar para o usuário messagens de feedback.
  
# Conclusão
Todos os endpoints propostos, foram utilizados, e foi feito algumas novas funcionalidades,
como ver perfil, proporcionar a possibilidade do usário sair do app. Validação dos
dados, no processo de login.